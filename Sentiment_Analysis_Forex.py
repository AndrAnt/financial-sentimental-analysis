#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd 
import numpy as np
from scipy import stats
from IPython.core.display import HTML
from sklearn import preprocessing
import matplotlib.pyplot as plt 
plt.rc("font", size=14)
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import seaborn as sns
from textblob import TextBlob, Word, Blobber
from textblob.classifiers import NaiveBayesClassifier
from textblob.taggers import NLTKTagger
from textblob.sentiments import NaiveBayesAnalyzer
import xlwt
import nltk
from nltk.tokenize import sent_tokenize
from nltk.tokenize import WordPunctTokenizer

sns.set(style="white")
sns.set(style="whitegrid", color_codes=True)
#importing necessary libraries

# reading and handling our first event file
MacroFile = open("EU All countries Edited.csv")
ForexFile = open("Gbp2Eur.csv")

# appending the data from the CSV File
MacroData = pd.read_csv(MacroFile)
ForexData = pd.read_csv(ForexFile)

display(MacroData), display(ForexData)

#Firstly, assign every forex change to the corresponding percentage and the corresponding date
#Data Format Column should be considered 


# In[2]:


# printing and checking the data of the file
MacroData['ï»¿Date'], ForexData['Date']
#NOTE: Some of the Dates that are missing from the ForexData are the Weekends, in which people CANNOT trade


# In[3]:


#Established the mean and standard deviation of GBP to Euro for the last 4 Years
ForexData['GBP'].describe()

#Type float64 is established for 4 decimal points in order to account for a pip - FOREX Trading


# In[4]:


MacroData.rename(columns={'ï»¿Date':'Date'}, inplace=True)
#Rename ï»¿Date to Date


# In[5]:


MacroData['Date'], ForexData['Date']


# In[6]:


#Identify headers for MacroData
list(MacroData)


# In[7]:


#Identify headers for ForexData
list(ForexData)


# In[8]:


MacroData['Macro_Event'].unique(), MacroData['Country_of_Event'].unique(),
#Figuring out the unique Occurances and considering the appropriate formulation of clusters


# In[9]:


#Merge and Call the files to check the results
MacroForex = pd.merge(MacroData, ForexData, left_on='Date', right_on='Date')
print(MacroForex)


# In[10]:


MacroForex.dtypes


# In[11]:


Event_Data = pd.to_numeric(MacroForex['Event_Data'], errors='coerce')
Event_Forecast_Data = pd.to_numeric(MacroForex['Event_Forecast_Data'], errors='coerce')
Date = pd.to_datetime(MacroForex['Date'])
Event_Data, Event_Forecast_Data, Date
#Change the type of the columns to the appropriate format and print to check the results. 
#Also, change any errors to "Null" for event data


# In[12]:


MaCrountryCluster = MacroForex.groupby(['Macro_Event','Country_of_Event']).Perc_Change.describe()
CountryCluster = MacroForex.groupby('Country_of_Event').Perc_Change.describe()
MacroEvent = MacroForex.groupby('Macro_Event').Perc_Change.describe()


# In[13]:


MaCrountryCluster, CountryCluster, MacroEvent
#check the variable of the macroeconomic events


# In[14]:


MacroForex.to_csv('MacroEvent.csv')
#Exported CSV File to Check data, share it with other team members and discuss the formulaton of the technical analysis


# In[15]:


MacroForex = pd.read_csv("MacroEvent.csv")


# In[16]:


MacroForex
#Check if file was loaded succesfully after potential changes


# In[17]:


sns.boxplot(x=MacroForex['Perc_Change'])
plt.show()
plt.savefig('count_plot')
#Create plot to check for any outliers.


# In[18]:


MacroForex['Perc_Change']= MacroForex["Perc_Change"].replace(100, 0) 
#Changing number "100%" - which means no change in day to day currency - to 0 (Removing that way the obvious outliers)


# In[19]:


Qi1 = MacroForex.quantile(0.25)
Qi3 = MacroForex.quantile(0.75)
IiQR = Qi3 - Qi1
print(IiQR)
#Check for further outliers


# In[20]:


MacroForex['Perc_Change']


# In[21]:


fig, ax = plt.subplots(figsize=(16,8))
ax.scatter(MacroForex['Perc_Change'], MacroForex['Country_of_Event'])
ax.set_xlabel('Change in Exchange Rate')
ax.set_ylabel('Country of Event')
plt.show()
#Check for other outliers. As seen, there are certain outliers


# In[22]:


MacroForex = MacroForex[np.abs(MacroForex.Perc_Change-MacroForex.Perc_Change.mean()) <= (3*MacroForex.Perc_Change.std())]
#remove outliers 3 standard eviations away from the mean


# In[23]:


MacroForex['Perc_Change'].hist(alpha=1, bins=50, figsize=(20,10))


# In[24]:


MacroForex.to_csv(r'MacroEvent.csv')
#Export new results to our csv file


# In[25]:


MacroForex = pd.read_csv("MacroEvent.csv")


# In[26]:


MacroForex


# In[27]:


MacroForex = MacroForex.drop("Unnamed: 0", axis=1)
#remove unncessary axis


# In[28]:


MacroForex


# In[29]:


MaCrountryCluster = MacroForex.groupby(['Macro_Event','Country_of_Event']).Perc_Change.describe()
CountryCluster = MacroForex.groupby('Country_of_Event').Perc_Change.describe()
MacroEvent = MacroForex.groupby('Macro_Event').Perc_Change.describe()
#Create the summary of statistics for the particular data


# In[30]:


MaCrountryCluster, CountryCluster, MacroEvent
#Create a statistical table, to view and examine the data


# In[31]:


list(MacroForex)
#List one more time all columns in the MacroForex table and decide which one would be optimal to use


# In[32]:


MacroPerc = MacroForex[['Macro_Event','Perc_Change']].copy()
#use copy function to avoid any changes affecting the main 


# In[33]:


MaCrountryCluster['mean'].hist(alpha=1, bins=50, figsize=(20,10))
#Check the macroeconomic events divided by country


# In[34]:


MaCrountryCluster.to_csv('MaCrountryCluster.csv')


# In[35]:


MaCrountryCluster = pd.read_csv("MaCrountryCluster.csv")


# In[36]:


list(MaCrountryCluster)


# In[37]:


MacroEvent = pd.read_csv("MacroEvent.csv")
MacroEvent = MacroEvent.drop("Unnamed: 0", axis=1)
list(MacroEvent)
#remove unnecessary columns and check again


# In[38]:


MeanDate = pd.merge(MacroEvent, MaCrountryCluster, left_on='Macro_Event', right_on='Macro_Event')
#merge the two files into a new file with everything so as to easily work through the file


# In[39]:


MeanDate.to_csv('MeanDate.csv')
#save the file and share with the team to check for any issues


# In[40]:


MeanDate = pd.read_csv('MeanDate.csv')
#an error is shown, although the data is read thoroughly. So the error is ignored.


# In[41]:


MeanDate = MeanDate.drop("Unnamed: 0", axis=1)
MeanDate
#Check data and drop any unusable columns


# In[42]:


data = pd.read_fwf('Construction_Output.txt')
data.columns = ['sentence','error']
#Download a Macroeconomic proceedings from https://www.ons.gov.uk/
#in order to import the file, it was easier to save as PDF first and then convert it from PDF to text within adobe


# In[43]:


data


# In[44]:


data.to_csv("construction_data.csv")
#export to check the data


# In[45]:


data['polarity'] = data.apply(lambda x: TextBlob(x['sentence']).sentiment.polarity, axis=1)
data['subjectivity'] = data.apply(lambda x: TextBlob(x['sentence']).sentiment.subjectivity, axis=1)
data = data[data != 0]
data
#apply sentimental analysis to the file


# In[46]:


data = data[np.isfinite(data['polarity'])]
data = data.dropna(1, thresh=1)
data
#Delete all "NaN" values from the dataset and print to check if this is done correctly


# In[47]:


Plot = data.plot(lw=2, colormap='jet', marker='.', markersize=10, title='Sentimental Analysis', figsize=(20,10))
Plot.set_xlabel("Number of Sentences")
Plot.set_ylabel("Sentimental Score")
#Create the plot with the specified labels


# In[48]:


pmean = data['polarity'].mean()
pmean
#get the mean of the the polarity


# In[49]:


url2='Construction_Output.txt'
infile2 = open(url2,'r')
sb = infile2.read().splitlines()
blob = Word(format(sb))
nphrases = TextBlob(format(blob.lemmatize))
wordcount = TextBlob(format(nphrases.noun_phrases))
print(wordcount.word_counts)

from wordcloud import WordCloud
wc = WordCloud().generate_from_frequencies(wordcount.word_counts)

plt.figure(figsize=(20,10))
plt.imshow(wc, interpolation="bilinear")
plt.axis("off")
plt.margins(x=0, y=0)
plt.show()


# In[50]:


MeanDate = pd.read_csv('MeanDate.csv')


# In[51]:


list(MeanDate)


# In[52]:


Date = '09/11/2018' #set the date of the event that you want to check DD/MM/YYYY
Country = 'United Kingdom' #set the country of the event that you want to check

checklist = MeanDate.loc[(MeanDate['Date'] == Date ) & (MeanDate['Country_of_Event_x'] == Country )]
#Check list to check the value of the event that you are looking for


# In[53]:


checklist
#Find the MacroEconomic event related to the file that you have mined


# In[54]:



Macro = 'U.K. Construction Output' #set the macroeconomic event that you want to check 

x = MeanDate.loc[(MeanDate['Country_of_Event_x'] == Country ) & (MeanDate['Macro_Event'] == Macro) & (MeanDate['Date'] == Date)]
#input the data of the event to each column


# In[55]:


x
#Check if the right event appears. Only one result should appear here.


# In[56]:


xmean = x['mean']
xsd = x['std']
xgbp = x['GBP']
#find the current mean of our data and the standard deviation of our data


# In[57]:


xforecast = (pmean * xsd)+xmean
#Forecast the probable percentage change for the data in the next day
fgbp = xgbp * (xforecast/100) + xgbp
#Forecast the actual gbp at the end of the day
fgbp


# In[58]:


checknextdate = '12/11/2018' 
#input the next date to check the GBP
#Please be careful, if this date is within a weekend, please choose Monday's date

checkprofit = MeanDate.loc[(MeanDate['Date'] == checknextdate )]
cgbp = checkprofit['GBP']
print(cgbp)
#


# In[59]:


Newgbp = 0.8756 #Input the price you found above
Profit = fgbp - xgbp
Profit
#An absolute value is needed, if both values are negative. In that case, the trader needs to SHORT and not BUY the currency


# In[ ]:




